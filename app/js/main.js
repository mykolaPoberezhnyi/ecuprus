$(document).ready(function() {
    $('.features-content').niceScroll({
        cursorcolor: "#83bfff",
        cursorwidth: "4px",
        background: "rgba(255,255,255,0)",
        cursorborder: "0px solid #83bfff",
        cursorborderradius: 0
    });    
    $('body').on('click', '#trigger-menu', function() {
        $('#fix-nav').toggleClass('active');
    });
    $('.carousel').carousel({
        interval: false
    })
    $('body').on('click', '#toTop', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    })
    $(".owl-carousel").owlCarousel({
        items: 4,
        margin: 30,
        loop: true,
        navs: true,
        dots: false,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });
    $('select').select2({
        width: 'style'
    });
    $('#signUp').on('shown.bs.modal', function() {
        $('.step1 ').find('.step-body').niceScroll({
            cursorcolor: "#83bfff",
            cursorwidth: "4px",
            background: "rgba(255,255,255,0)",
            cursorborder: "0px solid #83bfff",
            cursorborderradius: 0
        });
    })
    $('input[type=file]').on('change', function(){    
        const fileName = this.files[0].name;
        var label = $(this).next();
        fileName ? $(this).parent().addClass('is') : $(this).parent().removeClass('is');
        label.find('.isfile-title').text(fileName);
    });
    
    $('body').on('click', '.icon-close', function(e) {
        e.preventDefault();                
        var input = $(this).closest('.file').find('input[type=file]');
        input.val('');
        input.parent().removeClass('is');
    })
    $('body').on('click', '.modal .back', function(e) {
        e.preventDefault();
        var step = $(this).closest('.step');
        $('.step').removeClass('active');
        if (step.hasClass('step2')) {
            $('.step1').addClass('active');
            $('.step1').find('.step-body').niceScroll({
                cursorcolor: "#83bfff",
                cursorwidth: "4px",
                background: "rgba(255,255,255,0)",
                cursorborder: "0px solid #83bfff",
                cursorborderradius: 0
            });
        }
        else if (step.hasClass('step3')) {
            $('.step2').addClass('active');
            $('.step2').find('.step-body').niceScroll({
                cursorcolor: "#83bfff",
                cursorwidth: "4px",
                background: "rgba(255,255,255,0)",
                cursorborder: "0px solid #83bfff",
                cursorborderradius: 0
            });
        }
    })
    $('body').on('click', '.modal .next', function(e) {
        e.preventDefault();
        var step = $(this).closest('.step');
        $('.step').removeClass('active');
        if (step.hasClass('step1')) {
            $('.step2').addClass('active');
            $('.step2 ').find('.step-body').niceScroll({
                cursorcolor: "#83bfff",
                cursorwidth: "4px",
                background: "rgba(255,255,255,0)",
                cursorborder: "0px solid #83bfff",
                cursorborderradius: 0
            });
        }
        else if (step.hasClass('step2')) {
            $('.step3').addClass('active');
            $('.step2 ').find('.step-body').niceScroll({
                cursorcolor: "#83bfff",
                cursorwidth: "4px",
                background: "rgba(255,255,255,0)",
                cursorborder: "0px solid #83bfff",
                cursorborderradius: 0
            });
        }
    })
})

function initMap() {
    var coordinates = { lat: 40.7143528, lng: -74.0059731 };
    var map = new google.maps.Map(document.getElementById('map'), {
        center: coordinates,
        zoom: 9,
        disableDefaultUI: true,
        styles: [{
                "featureType": "all",
                "elementType": "geometry.fill",
                "stylers": [{
                    "weight": "2.00"
                }]
            },
            {
                "featureType": "all",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#9c9c9c"
                }]
            },
            {
                "featureType": "all",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "on"
                }]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{
                    "color": "#f2f2f2"
                }]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#7b7b7b"
                }]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [{
                    "visibility": "simplified"
                }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{
                        "color": "#46bcec"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#dae7e9"
                }]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#070707"
                }]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }]
            }
        ]
    });
    var image = 'img/marker.png';
    var marker = new google.maps.Marker({
        position: coordinates,
        map: map,
        icon: image
    });
}